﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using YouTube.Dto.User;
using YouTube.Interfaces.RepositoryStoreInterfaces;

namespace YouTube.Controllers
{
    public class UserController : ApiController
    {
        IUserRepositoryStore _userRepositoryStore;
        public UserController(IUserRepositoryStore repository)
        {
            _userRepositoryStore = repository;
        }

        private string UserIdentity()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return "anonymous";
            }
            else if (User.IsInRole("Admin"))
            {
                return "admin";
            }
            else
            {
                return "user";
            }
        }
        [Route("api/userProfile/{userId}")]
        public IHttpActionResult GetUser(string userId)
        {
            
            string userIdentity = UserIdentity();

            var user = _userRepositoryStore.GetUserById(userIdentity, userId, User.Identity.GetUserId());
           if(user.Info==null)
            {
                return NotFound();
            }
            return Ok(user);
        }

        [Authorize]
        [Route("api/updateUser/")]
        public IHttpActionResult PutUser(string userId,UserUpdateDto user)
        {
            if(user==null)
            {
                return BadRequest();
            }
            try
            {
                return Ok( _userRepositoryStore.UpdateUser(UserIdentity(), User.Identity.GetUserId(), userId, user));

            }
            catch
            {
                return BadRequest();
            }
        }
        [Authorize(Roles ="Admin")]
        [Route("api/blockUser/{userId}")]
        public IHttpActionResult GetBlock(string userId)
        {
            try
            {
                return Ok(_userRepositoryStore.BlockOrUnblock(UserIdentity(), userId));
            }
            catch
            {
                return BadRequest();
            }
        }


        [Authorize(Roles = "Admin")]
        [Route("api/users")]
        public IEnumerable<UsersDto> GetAll(string userId)
        {
            return _userRepositoryStore.GetallUsers(UserIdentity());
        }
    }
}
