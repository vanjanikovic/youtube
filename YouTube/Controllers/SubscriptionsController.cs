﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using YouTube.Dto.Subscription;
using YouTube.Interfaces.SubscriptionRepositoryInterfaces;

namespace YouTube.Controllers
{
    public class SubscriptionsController : ApiController
    {
        ISubscriptionRepository _repository;
        public SubscriptionsController(ISubscriptionRepository repository)
        {
            _repository = repository;
        }

        [Route("api/subs/{userId}")]
        public IEnumerable<SubscriptionDto> GetAllSubs(string userId)
        {
            return _repository.GetAllSubs(userId);
        }

        [Route("api/IsSubscribed/{userId}")]
        [Authorize]
        public bool GetSubs(string userId)
        {
            return _repository.IsSubscribed(userId, User.Identity.GetUserId());

        }

        [Route("api/Subscribe/{userId}")]
        [Authorize]
        public IHttpActionResult PostSub(string userId)
        {
            
            try
            {
                
                if (_repository.Subscribe(userId, User.Identity.GetUserId()))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return BadRequest();
            }
            
        }
        [Route("api/unsubscribe/{userId}")]
        [Authorize]
        public IHttpActionResult Delete(string userId)
        {
            try
            {
                _repository.Unsubscribe(userId, User.Identity.GetUserId());
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
