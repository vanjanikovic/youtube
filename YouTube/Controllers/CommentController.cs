﻿ using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using YouTube.Dto.Comment;
using YouTube.Interfaces.CommentRepositoryInterfaces;

namespace YouTube.Controllers
{
    public class CommentController : ApiController
    {
        ICommentRepository _repository;
        public CommentController(ICommentRepository repository)
        {
            _repository = repository;
        }
        private string UserIdentity()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return "anonymous";
            }
            else if (User.IsInRole("Admin"))
            {
                return "admin";
            }
            else
            {
                return "user";
            }
        }
        [Route("api/comment/{videoId}")]
        public IEnumerable<CommentDto> GetAllCommentsForVideo(int videoId)
        {
            return _repository.GetCommentsForVideo(UserIdentity(), User.Identity.GetUserId(), videoId);
        }
        [Authorize]
        [Route("api/comm")]

        public IHttpActionResult PostComment(CommentAddDto comment)
        {
            try
            {
               bool post= _repository.AddComment(User.Identity.GetUserId(),comment);
                if(post)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
