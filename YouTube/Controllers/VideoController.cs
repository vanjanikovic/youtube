﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using YouTube.Dto.Video;
using YouTube.Interfaces.RepositoryStoreInterfaces;

namespace YouTube.Controllers
{
    public class VideoController : ApiController
    {
        IVideoRepositoryStore _repositoryStore;
        public VideoController(IVideoRepositoryStore repositoryStore)
        {
            _repositoryStore = repositoryStore;
        }

        private string UserIdentity()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return "anonymous";
            }
            else if (User.IsInRole("Admin"))
            {
                return "admin";
            }
            else
            {
                return "user";
            }
        }
        [Route("api/videos")]
        public IEnumerable<VideoDto> GetAllVideos()
        {
            string identity = UserIdentity();
            return _repositoryStore.GetAll(identity, User.Identity.GetUserId());

        }

        [Route("api/userVideos/{userId}")]
        public IEnumerable<VideoDto> GetAllUserVideos(string userId)
        {
            string identity = UserIdentity();
            return _repositoryStore.GetAllUserVideos(identity, userId, User.Identity.GetUserId());

        }
        [Route("api/video/{videoId}")]
        public IHttpActionResult GetVideo(int videoId)
        {
            string identity = UserIdentity();
            var video = _repositoryStore.GetVideo(identity, User.Identity.GetUserId(), videoId);
            if (video.Info == null)
            {
                return BadRequest();
            }
            else return Ok(video);
        }
        [Route("api/video")]
        [Authorize]
        public IHttpActionResult PostVideo(VideoAddDto video)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                string userIdentity = UserIdentity();
                _repositoryStore.AddVideo(userIdentity, User.Identity.GetUserId(), video);
            }
            catch
            {
                return BadRequest("nije ga dodalo");
            }
            return Ok();
        }

        [Route("api/video/{videoId}")]
        [Authorize]
        public IHttpActionResult PutVideo(int videoId, VideoUpdateDto video)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (video == null)
            {
                return NotFound();
            }
            if (videoId != video.Id)
            {

                return BadRequest();
            }

            try
            {
                string userIdentity = UserIdentity();
                _repositoryStore.UpdateVideo(userIdentity, User.Identity.GetUserId(), video);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }

        [Route("api/video/{videoId}")]
        [Authorize]
        public IHttpActionResult DeleteVideo(int videoId)
        {

            try
            {
                string userIdentity = UserIdentity();
                _repositoryStore.DeleteVideo(userIdentity, User.Identity.GetUserId(), videoId);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }

        [Route("api/videoBlock/{videoId}")]
        [Authorize(Roles ="Admin")]
        public IHttpActionResult PutBlockVideo(int videoId)
        {
            try
            {
                _repositoryStore.BlockVideo(UserIdentity(), videoId);
            }
            catch
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
