﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using YouTube.Interfaces.LikeRepositoryInterfaces;

namespace YouTube.Controllers
{
    public class LikeController : ApiController
    {
        ILikeRepositoryInterface _repository;
        public LikeController(ILikeRepositoryInterface repository)
        {
            _repository = repository;
        }

        [Route("api/videoRating/{videoId}")]
        public IHttpActionResult GetVideoRating(int videoId)
        {
            return Ok(_repository.GetVideoRating(videoId));
        }
    }
}
