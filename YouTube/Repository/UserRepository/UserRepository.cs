﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Models;

namespace YouTube.Repository.UserRepository
{
    public class UserRepository:IDisposable
    {
        protected ApplicationDbContext db = new ApplicationDbContext();
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}