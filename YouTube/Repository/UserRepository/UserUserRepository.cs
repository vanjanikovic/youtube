﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.User;
using YouTube.Interfaces.UserRepositoryInterfaces;

namespace YouTube.Repository.UserRepository
{
    public class UserUserRepository : UserRepository, IUserRepository,IUpdateUserRepository
    {
        public UserDto GetUserById(string userId, string thisUserId)
        {
            var user = db.Users.FirstOrDefault(x => x.Id == userId && !x.IsDeleted);
            if (user == null)
            {
                return new UserDto();
            }
            if (user.Id==thisUserId)
                {
                    return new UserDetailsDto()
                    {
                        Id = user.Id,
                        Username = user.UserName,
                        Info = "owner",
                        CreatedDate = user.JoinDate,
                        Description = user.Description,
                        NumberOfSubs = db.Subscriptions.Where(x => x.UserId == user.Id).Count(),
                        IsBlocked=user.IsBlocked
                    };

                }
                if (user.IsBlocked)
                {
                    return new UserDto()
                    {
                        Id = user.Id,
                        Info = "User is blocked",
                        IsBlocked=true
                    };
                }
                return new UserDetailsDto()
                {
                    Id = user.Id,
                    Username = user.UserName,
                    Info = "user",
                    CreatedDate = user.JoinDate,
                    Description = user.Description,
                    NumberOfSubs = db.Subscriptions.Where(x => x.UserId == user.Id).Count(),
                    IsBlocked=false
                };

        }

        public bool UpdateUser(string thisUserId, string userId, UserUpdateDto user)
        {
            if(thisUserId!= userId || thisUserId!=user.Id)
            {
                return false;
            }
            var oldUser = db.Users.FirstOrDefault(x => x.Id == userId && !x.IsDeleted);
            if(oldUser==null)
            {
                return false;
            }

            //update.. missing update password!
            oldUser.FirstName = user.FirstName=="" ? user.FirstName : oldUser.FirstName;
            oldUser.LastName = user.LastName=="" ? user.LastName : oldUser.LastName;
            oldUser.Description = user.Description == "" ? user.Description : oldUser.Description;

            db.Entry(oldUser).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch
            {
                throw;
            }

            return true;


        }
    }
}