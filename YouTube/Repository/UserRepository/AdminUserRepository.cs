﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.User;
using YouTube.Interfaces.UserRepositoryInterfaces;
using YouTube.Models;

namespace YouTube.Repository.UserRepository
{

    public class AdminUserRepository : UserRepository, IUserRepository,IGetAllUsersRepository,IDeleteUserRepository,IBlockUserRepository,IUpdateUserRepository
    {
        public bool BlockUnblockUser(string userId)
        {
            var user = db.Users.FirstOrDefault(x => x.Id == userId && !x.IsDeleted);
            if(user==null)
            {
                return false;
            }
            user.IsBlocked = user.IsBlocked ? false : true;
            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch
            {
                 throw;
            }
            return true;
        }

        public IEnumerable<UsersDto> GetAllUsers()
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            return db.Users.Where(x=>!x.IsDeleted).Select(x => new UsersDto() { 
                Id=x.Id,
                Username=x.UserName,
                FirstName=x.FirstName,
                LastName=x.LastName,
                JoinDate=x.JoinDate,
                Role= manager.IsInRole(x.Id, "Admin") ? "Admin" : "User",
                Email=x.Email
            });
        }

        public UserDto GetUserById(string userId, string thisUserId)
        {
            var user = db.Users.FirstOrDefault(x => x.Id == userId && !x.IsDeleted);
            if (user == null)
            {
                return new UserDto();
            }
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

                return new UserAdminDetailsDto()
                {
                    Id = user.Id,
                    Username = user.UserName,
                    Info = "admin",
                    CreatedDate = user.JoinDate,
                    Description = user.Description,
                    NumberOfSubs = db.Subscriptions.Where(x => x.UserId == user.Id).Count(),
                    IsBlocked = user.IsBlocked,
                    Role=manager.IsInRole(user.Id,"Admin")? "Admin":"User"
                };
           
            
        }

        public bool UpdateUser(string thisUserId, string userId, UserUpdateDto user)
        {
            if (user.Id !=userId)
            {
                return false;
            }
            var oldUser = db.Users.FirstOrDefault(x => x.Id == userId && !x.IsDeleted);
            if (oldUser == null)
            {
                return false;
            }

            //update.. missing update password!
            oldUser.FirstName = user.FirstName == "" ? user.FirstName : oldUser.FirstName;
            oldUser.LastName = user.LastName == "" ? user.LastName : oldUser.LastName;
            oldUser.Description = user.Description == "" ? user.Description : oldUser.Description;

            db.Entry(oldUser).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch
            {
                throw;
            }

            return true;

        }
    }
}