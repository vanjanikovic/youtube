﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.User;
using YouTube.Interfaces.UserRepositoryInterfaces;

namespace YouTube.Repository.UserRepository
{
    public class AnonymousUserRepository : UserRepository, IUserRepository
    {
        public UserDto GetUserById(string userId, string thisUserId)
        {
            var user = db.Users.FirstOrDefault(x => x.Id == userId && !x.IsDeleted);
            if(user==null)
            {
                return new UserDto();
            }
           
                if (user.IsBlocked)
                {
                    return new UserDto()
                    {
                        Id = user.Id,
                        Info = "User is blocked",
                        IsBlocked=true
                        
                    };
                }
                return new UserDetailsDto()
                {
                    Id = user.Id,
                    Username = user.UserName,
                    Info = "anonymous",
                    CreatedDate = user.JoinDate,
                    Description = user.Description,
                    NumberOfSubs = db.Subscriptions.Where(x => x.UserId == user.Id).Count(),
                    IsBlocked = false

                };
               
            
            
        }
    }
}