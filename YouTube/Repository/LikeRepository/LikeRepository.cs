﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.Like;
using YouTube.Interfaces.LikeRepositoryInterfaces;
using YouTube.Models;

namespace YouTube.Repository.LikeRepository
{
    public class LikeRepository : ILikeRepositoryInterface,IDisposable
    {
        protected ApplicationDbContext db = new ApplicationDbContext();
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public LikeDto GetVideoRating(int videoId)
        {
            var likes= db.Likes.Where(x => x.VideoId == videoId);
            var rating = new LikeDto() {
                VideoOrCommentId = videoId,
                Likes = likes.Where(x => x.IsLike).Count(),
                Dislikes=likes.Where(x=>!x.IsLike).Count()
            };
            return rating;
        }
    }
}