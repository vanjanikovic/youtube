﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.Comment;
using YouTube.Interfaces.CommentRepositoryInterfaces;
using YouTube.Models;

namespace YouTube.Repository.CommentRepository
{
    public class CommentRepository : ICommentRepository,IDisposable
    {
        protected ApplicationDbContext db = new ApplicationDbContext();
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public IEnumerable<CommentDto> GetCommentsForVideo(string userIdentity, string thisUserId,int videoId)
        {
            if(userIdentity=="admin")
            {
                var comments = db.Comments.Include("User").Where(x => x.VideoId == videoId);
                return comments.Select(x => new CommentDto()
                {
                    Id = x.Id,
                    DateCreated = x.DateCreated,
                    Text = x.Text,
                    UserId = x.UserId,
                    Username = x.User.UserName,
                    Likes = db.Likes.Where(y => y.CommentId == x.Id && y.IsLike).Count(),
                    Dislikes=db.Likes.Where(y=>y.CommentId==x.Id && !y.IsLike).Count()
                });
            }
            else if(userIdentity=="user")
            {
                var comments = db.Comments.Include("Video").Where(x => x.VideoId == videoId);
                var video = db.Videos.FirstOrDefault(x => x.Id == videoId);
                if(!video.AllowComments)
                {
                    if(video.UserId==thisUserId)
                    {
                        return comments.Select(x=>new CommentDto() {
                            Id = x.Id,
                            DateCreated = x.DateCreated,
                            Text = x.Text,
                            UserId = x.UserId,
                            Username = x.User.UserName,
                            Likes = db.Likes.Where(y => y.CommentId == x.Id && y.IsLike).Count(),
                            Dislikes = db.Likes.Where(y => y.CommentId == x.Id && !y.IsLike).Count()
                        });
                    }
                    else
                    {
                        return new List<CommentDto>();
                    }
                }

                return comments.Select(x => new CommentDto()
                {
                    Id = x.Id,
                    DateCreated = x.DateCreated,
                    Text = x.Text,
                    UserId = x.UserId,
                    Username = x.User.UserName,
                    Likes = db.Likes.Where(y => y.CommentId == x.Id && y.IsLike).Count(),
                    Dislikes = db.Likes.Where(y => y.CommentId == x.Id && !y.IsLike).Count()
                });
            }
            else
            {
                var comments = db.Comments.Include("Video").Where(x => x.VideoId == videoId);
                var video = db.Videos.FirstOrDefault(x => x.Id == videoId);
                if(video.AllowComments==false)
                {
                    return new List<CommentDto>();
                }
                return comments.Select(x => new CommentDto()
                {
                    Id = x.Id,
                    DateCreated = x.DateCreated,
                    Text = x.Text,
                    UserId = x.UserId,
                    Username = x.User.UserName,
                    Likes = db.Likes.Where(y => y.CommentId == x.Id && y.IsLike).Count(),
                    Dislikes = db.Likes.Where(y => y.CommentId == x.Id && !y.IsLike).Count()
                });
            }
             
         }

        public bool AddComment(string thisUserId, CommentAddDto comment)
        {
            var video = db.Videos.FirstOrDefault(x => x.Id == comment.VideoId);
            if(video==null)
            {
                return false;
            }
            if(!video.AllowComments)
            {
                return false;
            }
            var comm = new Comment() { 
                Text=comment.Text,
                DateCreated=DateTime.Now,
                UserId=thisUserId,
                IsDeleted=false,
                VideoId=comment.VideoId
            };
            db.Comments.Add(comm);
            try
            {
                db.SaveChanges();
            }
            catch
            {
                throw;
            }
            return true;
        }
    }
}