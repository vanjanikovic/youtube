﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.Video;
using YouTube.Interfaces.VideoRepositoryInterfaces;
using YouTube.Models;

namespace YouTube.Repository.VideoRepository
{
    public class AdminVideoRepository : VideoRepository, IVideoRepository,IAddVideoRepository,IUpdateVideoRepository,IDeleteVideoRepository,IBlockVideoRepository
    {
        public IEnumerable<VideoDto> GetAll(string userId)
        {
            var videos = db.Videos.Include("User").Where(x => !x.User.IsDeleted && !x.IsDeleted ).
                 Select(x => new VideoDto
                 {
                     Id = x.Id,
                     Name = x.Name,
                     UserId = x.UserId,
                     UserName = x.User.UserName,
                     DateCreated = x.DateCreated,
                     ThumbnailSrc = x.ThumbnailSrc,
                     Rating = x.Rating,
                     Info = "admin"
                 });
            return videos;
        }

        public IEnumerable<VideoDto> GetAllUserVideos(string userId, string thisUserId)
        {
            var videos = db.Videos.Include("User").Where(x => !x.User.IsDeleted && x.UserId == userId && !x.IsDeleted).
                 Select(x => new VideoDto
                 {
                     Id = x.Id,
                     Name = x.Name,
                     UserId = x.UserId,
                     UserName = x.User.UserName,
                     DateCreated = x.DateCreated,
                     ThumbnailSrc = x.ThumbnailSrc,
                     Rating = x.Rating,
                     Info = "admin"
                 }).OrderByDescending(x => x.DateCreated);
            return videos;
        }

        public VideoDetailsDto GetVideo(string userId, int videoId)
        {
            var video = db.Videos.Include("User").FirstOrDefault(x => x.Id == videoId && !x.IsDeleted);
            if (video == null)
            {
                return new VideoDetailsDto();
            }
            
                return new VideoDetailsDto()
                {

                    Id = video.Id,
                    Info = "admin",
                    Name = video.Name,
                    AllowRating = video.AllowRating,
                    AllowComments = video.AllowComments,
                    VideoSrc = video.VideoSrc,
                    ThumbnailSrc = video.ThumbnailSrc,
                    IsDeleted = video.IsBlocked,
                    IsBlocked = video.IsDeleted,
                    UserId = video.UserId,
                    Username = video.User.UserName,
                    Rating = video.Rating,
                    DateCreated = video.DateCreated,
                    Description = video.Description
                };
        }
        public void AddVideo(string userID, VideoAddDto video)
        {

            var newVideo = new Video()
            {
                Name = video.Name,
                VideoSrc = video.VideoSrc,
                ThumbnailSrc = video.ThumbnailSrc,
                VisibilityId = video.VisibilityId==0? 1 : video.VisibilityId ,
                AllowComments = true,
                AllowRating = true,
                IsBlocked = false,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                UserId = userID
            };
            db.Videos.Add(newVideo);
            try
            {
                db.SaveChanges();
            }
            catch
            {
                throw;

            }
            

        }

        public void UpdateVideo(string userId, VideoUpdateDto video)
        {

            var oldVideo = db.Videos.FirstOrDefault(x => x.Id == video.Id);
            oldVideo.Description = video.Description;
            oldVideo.VisibilityId = video.VisibilityId == 0 ? oldVideo.VisibilityId : video.VisibilityId;
            oldVideo.AllowComments = video.AllowComments;
            oldVideo.AllowRating = video.AllowRating;
            db.Entry(oldVideo).State = System.Data.Entity.EntityState.Modified; ;
            try
            {
                db.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void Delete(string userId, int videoId)
        {
            var video = db.Videos.FirstOrDefault(x =>  x.Id == videoId);
            

            try
            {
                video.IsDeleted = true;
                db.Entry(video).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void BlockOrUnblock(int videoId)
        {
            var video = db.Videos.FirstOrDefault(x =>  x.Id == videoId);
            video.IsBlocked = !video.IsBlocked;
            db.Entry(video).State = System.Data.Entity.EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}