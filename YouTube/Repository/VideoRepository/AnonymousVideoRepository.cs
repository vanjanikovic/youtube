﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.Video;
using YouTube.Interfaces.VideoRepositoryInterfaces;

namespace YouTube.Repository.VideoRepository
{
    public class AnonymousVideoRepository : VideoRepository, IVideoRepository
    {
        public IEnumerable<VideoDto> GetAll(string userId)
        {
            var videos = db.Videos.Include("User").Where(x => !x.User.IsBlocked && !x.User.IsDeleted && !x.IsBlocked && !x.IsDeleted && x.VisibilityId == 1).
                 Select(x => new VideoDto
                 {
                     Id=x.Id,
                     Name=x.Name,
                     UserId=x.UserId,
                     UserName=x.User.UserName,
                     DateCreated=x.DateCreated,
                     ThumbnailSrc=x.ThumbnailSrc,
                     Rating=x.Rating,
                     Info="anonymous"
                 });
            return videos;
        }

        public IEnumerable<VideoDto> GetAllUserVideos(string userId, string thisUserId)
        {
            var videos = db.Videos.Include("User").Where(x => x.UserId==userId && !x.User.IsBlocked && !x.User.IsDeleted && !x.IsBlocked && !x.IsDeleted && x.VisibilityId == 1).
                  Select(x => new VideoDto
                  {
                      Id = x.Id,
                      Name = x.Name,
                      UserId = x.UserId,
                      UserName = x.User.UserName,
                      DateCreated = x.DateCreated,
                      ThumbnailSrc = x.ThumbnailSrc,
                      Rating = x.Rating,
                      Info = "anonymous"
                  }).OrderByDescending(x=>x.DateCreated);
            return videos;
        }

        public VideoDetailsDto GetVideo(string userId, int videoId)
        {
            var video = db.Videos.Include("User").FirstOrDefault(x => x.Id == videoId && !x.IsDeleted);
            if (video == null)
            {
                return new VideoDetailsDto();
            }
            
                if (video.IsBlocked)
                {
                    return new VideoDetailsDto()
                    {
                        Id = video.Id,
                        Info = "Video is blocked",
                        Name = video.Name,
                        IsBlocked = true
                    };
                }
                if (!(video.VisibilityId == 1))
                {
                    return new VideoDetailsDto()
                    {
                        Id = video.Id,
                        Info = "Video is not visible",
                        Name = video.Name

                    };
                }
                if (video.User.IsBlocked)
                {
                    return new VideoDetailsDto()
                    {
                        Id = video.Id,
                        Info = "Owner of video is blocked",
                        Name = video.Name
                    };
                }
                if (video.User.IsDeleted)
                {
                    return new VideoDetailsDto()
                    {
                        Id = video.Id,
                        Info = "Owner of video is deleted",
                        Name = video.Name

                    };
                }

            video.Rating = video.Rating + 1;
            db.Entry(video).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

            return new VideoDetailsDto()
            {
                Id = video.Id,
                Info = "anonymous",
                Name = video.Name,
                AllowRating = video.AllowRating,
                AllowComments = video.AllowComments,
                VideoSrc = video.VideoSrc,
                ThumbnailSrc = video.ThumbnailSrc,
                IsDeleted = video.IsDeleted,
                IsBlocked = video.IsBlocked,
                UserId = video.UserId,
                Username = video.User.UserName,
                Rating = video.Rating,
                DateCreated = video.DateCreated,
                Description = video.Description


            };
        }
    }
}