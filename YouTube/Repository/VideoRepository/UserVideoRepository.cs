﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.Video;
using YouTube.Interfaces.VideoRepositoryInterfaces;
using YouTube.Models;

namespace YouTube.Repository.VideoRepository
{

    public class UserVideoRepository : VideoRepository, IVideoRepository,IAddVideoRepository,IUpdateVideoRepository,IDeleteVideoRepository
    {
        public void AddVideo(string userID, VideoAddDto video)
        {

            var newVideo = new Video()
            {
             
                Name = video.Name,
                VideoSrc = video.VideoSrc,
                ThumbnailSrc = video.ThumbnailSrc,
                VisibilityId = video.VisibilityId == 0 ? 1 : video.VisibilityId,
                AllowComments = true,
                AllowRating = true,
                IsBlocked = false,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                UserId = userID
            };
            db.Videos.Add(newVideo);
            try
            {
                db.SaveChanges();

            }
            catch(Exception e)
            {
                throw;

            }
        }

        public void Delete(string userId, int videoId)
        {
            var video = db.Videos.FirstOrDefault(x => x.UserId == userId && x.Id == videoId);
           

            try
            {
                video.IsDeleted = true;
                db.Entry(video).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<VideoDto> GetAll(string userId)
        {
            var videos = db.Videos.Include("User").Where(x =>(x.UserId==userId && !x.IsDeleted) ||(!x.User.IsBlocked && !x.User.IsDeleted && !x.IsBlocked && !x.IsDeleted && x.VisibilityId == 1)).
                Select(x => new VideoDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    UserId = x.UserId,
                    UserName = x.User.UserName,
                    DateCreated = x.DateCreated,
                    ThumbnailSrc = x.ThumbnailSrc,
                    Rating = x.Rating,
                    Info = "user"
                });
            return videos;
        }

        public IEnumerable<VideoDto> GetAllUserVideos(string userId, string thisUserId)
        {
            if(userId==thisUserId)
            {
                var videos = db.Videos.Include("User").Where(x => x.UserId == userId && !x.IsDeleted).
                 Select(x => new VideoDto
                 {
                     Id = x.Id,
                     Name = x.Name,
                     UserId = x.UserId,
                     UserName = x.User.UserName,
                     DateCreated = x.DateCreated,
                     ThumbnailSrc = x.ThumbnailSrc,
                     Rating = x.Rating,
                     Info = "owner"
                 }).OrderByDescending(x => x.DateCreated);
                return videos;
            }
            else
            {
                var videos = db.Videos.Include("User").Where(x => x.UserId == userId && !x.User.IsBlocked && !x.User.IsDeleted && !x.IsBlocked && !x.IsDeleted && x.VisibilityId == 1).
                 Select(x => new VideoDto
                 {
                     Id = x.Id,
                     Name = x.Name,
                     UserId = x.UserId,
                     UserName = x.User.UserName,
                     DateCreated = x.DateCreated,
                     ThumbnailSrc = x.ThumbnailSrc,
                     Rating = x.Rating,
                     Info = "user"
                 }).OrderByDescending(x => x.DateCreated);
                return videos;
            }
        }

        public VideoDetailsDto GetVideo(string userId, int videoId)
        {
            var video = db.Videos.Include("User").FirstOrDefault(x => x.Id == videoId && !x.IsDeleted);
            if (video == null)
            {
                return new VideoDetailsDto();
            }
            
                if (video.UserId == userId)
                {
                    return new VideoDetailsDto()
                    {

                        Id = video.Id,
                        Info = "owner",
                        Name = video.Name,
                        AllowRating = video.AllowRating,
                        AllowComments = video.AllowRating,
                        VideoSrc = video.VideoSrc,
                        ThumbnailSrc = video.ThumbnailSrc,
                        IsDeleted = video.IsDeleted,
                        IsBlocked = video.IsBlocked,
                        UserId = video.UserId,
                        Username = video.User.UserName,
                        Rating = video.Rating,
                        DateCreated = video.DateCreated,
                        Description = video.Description
                    };
                }
                else
                {

                    if (video.IsBlocked)
                    {
                        return new VideoDetailsDto()
                        {
                            Id = video.Id,
                            Info = "Video is blocked",
                            Name = video.Name,
                            IsBlocked = true
                        };
                    }
                    
                    if (!(video.VisibilityId == 1))
                    {
                        return new VideoDetailsDto()
                        {
                            Id = video.Id,
                            Info = "Video is not visible",
                            Name = video.Name

                        };
                    }
                    if (video.User.IsBlocked)
                    {
                        return new VideoDetailsDto()
                        {
                            Id = video.Id,
                            Info = "Owner of video is blocked",
                            Name = video.Name
                        };
                    }
                    if (video.User.IsDeleted)
                    {
                        return new VideoDetailsDto()
                        {
                            Id = video.Id,
                            Info = "Owner of video is deleted",
                            Name = video.Name

                        };
                    }

                video.Rating = video.Rating + 1;
                db.Entry(video).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return new VideoDetailsDto()
                    {
                        Id = video.Id,
                        Info = "user",
                        Name = video.Name,
                        AllowRating = video.AllowRating,
                        AllowComments = video.AllowComments,
                        VideoSrc = video.VideoSrc,
                        ThumbnailSrc = video.ThumbnailSrc,
                        IsDeleted = video.IsDeleted,
                        IsBlocked = video.IsBlocked,
                        UserId = video.UserId,
                        Username = video.User.UserName,
                        Rating = video.Rating,
                        DateCreated = video.DateCreated,
                        Description = video.Description


                    };
                }
        }

        public void UpdateVideo(string userId, VideoUpdateDto video)
        {
            var oldVideo = db.Videos.FirstOrDefault(x => x.Id == video.Id && x.UserId == userId);
            oldVideo.Description = video.Description;
            oldVideo.VisibilityId = video.VisibilityId == 0 ? oldVideo.VisibilityId : video.VisibilityId;
            oldVideo.AllowComments = video.AllowComments;
            oldVideo.AllowRating = video.AllowRating;
            db.Entry(oldVideo).State = System.Data.Entity.EntityState.Modified; 
            try
            {
                db.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}