﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.Subscription;
using YouTube.Interfaces.SubscriptionRepositoryInterfaces;
using YouTube.Models;

namespace YouTube.Repository.SubscriptionRepository
{
    public class SubscriptionRepository:IDisposable,ISubscriptionRepository
    {
        protected ApplicationDbContext db = new ApplicationDbContext();
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<SubscriptionDto> GetAllSubs(string userId)
        {
            var subs = db.Subscriptions.Include("User").Where(x => x.SubscribedUserId == userId)
                .Select(x => new SubscriptionDto()
                {
                    Id=x.Id,
                    UserId=x.UserId,
                    UserName=x.User.UserName,
                    NumberOfSubscriptions=db.Subscriptions.Where(y=>y.UserId==x.UserId).Count()
                });
            return subs;
        }

        public bool IsSubscribed(string userId, string thisUserId)
        {
            var sub = db.Subscriptions.FirstOrDefault(x => x.UserId == userId && x.SubscribedUserId == thisUserId);
            if(sub==null)
            {
                return false;
            }
            return true;
        }

        public bool Subscribe(string userId, string thisUserId)
        {
            var subExists = db.Subscriptions.FirstOrDefault(x => x.UserId == userId && x.SubscribedUserId == thisUserId);
            if (subExists != null)
            {
                return false;
            }
           
            var sub = new Subscription() { 
                UserId=userId,
                SubscribedUserId=thisUserId
            };
            db.Subscriptions.Add(sub);
            try {
                db.SaveChanges();
            }
            catch
            {
                throw;
            }
            return true;
        }

        public void Unsubscribe(string userId, string thisUserId)
        {
            var sub = db.Subscriptions.FirstOrDefault(x => x.UserId == userId && x.SubscribedUserId == thisUserId);
            db.Subscriptions.Remove(sub);
            try
            {
                db.SaveChanges();
            }
            catch
            {
                throw;
            }

        }
    }
}