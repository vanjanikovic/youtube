﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.Video;
using YouTube.Interfaces.RepositoryStoreInterfaces;
using YouTube.Interfaces.VideoRepositoryInterfaces;
using YouTube.Repository.VideoRepository;

namespace YouTube.Repository.RepositoryStore
{
    public class VideoRepositoryStore:IVideoRepositoryStore
    {
        private IVideoRepository _videoRepository;
        private IAddVideoRepository _addVideoRepository;
        private IUpdateVideoRepository _updateVideoRepository;
        private IDeleteVideoRepository _deleteVideoRepository;
        private IBlockVideoRepository _blockVideoRepo;
        private void SetBlockVideoRepository(string userIdentity)
        {
            if(userIdentity=="admin")
            {
                _blockVideoRepo = new AdminVideoRepository();
            }
        }
        private void SetVideoRepository(string userIdentity)
        {
            if(userIdentity=="admin")
            {
                _videoRepository = new AdminVideoRepository();
                
            }
            else if(userIdentity=="user")
            {
                _videoRepository = new UserVideoRepository();
               

            }
            else
            {
                _videoRepository = new AnonymousVideoRepository();
            }
        }
        private void SetAddVideoRepository(string userIdentity)
        {
            if(userIdentity=="admin")
            {
                _addVideoRepository = new AdminVideoRepository();
            }
            else
            {
                _addVideoRepository = new UserVideoRepository();
            }
        }
        private void SetUpdateVideoRepository(string userIdentity)
        {
            if(userIdentity=="admin")
            {
                _updateVideoRepository = new AdminVideoRepository();
            }
            else
            {
                _updateVideoRepository = new UserVideoRepository();
            }
        }
        private void SetDeleteVideoRepository(string userIdentity)
        {
            if(userIdentity=="admin")
            {
                _deleteVideoRepository = new AdminVideoRepository();

            }
            else
            {
                _deleteVideoRepository = new UserVideoRepository();
            }
        }
        public IEnumerable<VideoDto> GetAll(string userIdentity, string userId)
        {
            SetVideoRepository(userIdentity);
            return _videoRepository.GetAll(userId);

        }

        public IEnumerable<VideoDto> GetAllUserVideos(string userIdentity, string userId, string thisUserId)
        {
            SetVideoRepository(userIdentity);
            return _videoRepository.GetAllUserVideos(userId, thisUserId);
        }

        public VideoDetailsDto GetVideo(string userIdentity, string userId, int videoId)
        {
           SetVideoRepository(userIdentity);
            return _videoRepository.GetVideo(userId, videoId);
        }
        public void AddVideo(string userIdentity,string userId,VideoAddDto video)
        {
            SetAddVideoRepository(userIdentity);
            try
            {
                _addVideoRepository.AddVideo(userId, video);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateVideo(string userIdentity, string userId, VideoUpdateDto video)
        {
            SetUpdateVideoRepository(userIdentity);
            try
            {
                _updateVideoRepository.UpdateVideo(userId, video);
            }
            catch
            {
                throw;
            }
        }

        public void DeleteVideo(string userIdentity, string userId, int videoId)
        {
            SetDeleteVideoRepository(userIdentity);
            try
            {
                _deleteVideoRepository.Delete(userId, videoId);
            }
            catch
            {
                throw;
            }
        }

        public void BlockVideo(string userIdentity, int videoId)
        {
            SetBlockVideoRepository(userIdentity);
            try
            {
                _blockVideoRepo.BlockOrUnblock(videoId);
            }
            catch
            {
                throw;
            }
        }
    }
}