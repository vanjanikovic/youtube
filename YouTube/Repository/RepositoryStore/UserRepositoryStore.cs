﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouTube.Dto.User;
using YouTube.Interfaces.RepositoryStoreInterfaces;
using YouTube.Interfaces.UserRepositoryInterfaces;
using YouTube.Repository.UserRepository;

namespace YouTube.Repository.RepositoryStore
{
    public class UserRepositoryStore:IUserRepositoryStore
    {
        private IUserRepository _userRepository;
        private IGetAllUsersRepository _getAllUsersRepository;
        private IBlockUserRepository _blockUserRepository;
        private IDeleteUserRepository _deleteUserRepository;
        private IUpdateUserRepository _updateUserRepository;
        private void SetUpdateUserRepository(string userIdentity)
        {
            if (userIdentity == "admin")
            {
                _updateUserRepository = new AdminUserRepository();
            }
            else if (userIdentity == "user")
            {
                _updateUserRepository = new UserUserRepository();
            }


        }
        private void SetDeleteUserRepository(string userIdentity)
        {
            if(userIdentity=="admin")
            {
                _deleteUserRepository = new AdminUserRepository();
            }
        }

        private void SetBlockUserrepository(string userIdentity)
        {
            if(userIdentity=="admin")
            {
                _blockUserRepository = new AdminUserRepository();
            }
        }
        private void SetGetAllUsersRepository(string userIdentity)
        {
            if(userIdentity=="admin")
            {
                _getAllUsersRepository = new AdminUserRepository();
            }
        }
        private void SetUserRepository(string userIdentity)
        {
            if(userIdentity=="admin")
            {
                _userRepository = new AdminUserRepository();
            }
            else if(userIdentity=="user")
            {
                _userRepository = new UserUserRepository();
            }
            else
            {
                _userRepository = new AnonymousUserRepository();
            }

        }

        public UserDto GetUserById(string userIdentity, string userId, string thisUserId)
        {
            SetUserRepository(userIdentity);
            return _userRepository.GetUserById(userId, thisUserId);
        }

        public bool UpdateUser(string userIdentity, string thisUserId, string userId, UserUpdateDto user)
        {
            SetUpdateUserRepository(userIdentity);
            try
            {
                return _updateUserRepository.UpdateUser(thisUserId, userId, user);
            }
            catch
            {
                throw;
            }
        }

        public bool BlockOrUnblock(string userIdentity, string userId)
        {
            SetBlockUserrepository(userIdentity);
            try
            {
                return _blockUserRepository.BlockUnblockUser(userId);
            }
            catch
            {
                throw;
            }
        }

        public IEnumerable<UsersDto> GetallUsers(string userIdentity)
        {
            SetGetAllUsersRepository(userIdentity);
            return _getAllUsersRepository.GetAllUsers();
        }
    }
}