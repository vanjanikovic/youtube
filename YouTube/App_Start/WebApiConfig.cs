﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using ProductService.Resolver;
using Unity;
using Unity.Lifetime;
using YouTube.Interfaces.CommentRepositoryInterfaces;
using YouTube.Interfaces.LikeRepositoryInterfaces;
using YouTube.Interfaces.RepositoryStoreInterfaces;
using YouTube.Interfaces.SubscriptionRepositoryInterfaces;
using YouTube.Repository.CommentRepository;
using YouTube.Repository.LikeRepository;
using YouTube.Repository.RepositoryStore;
using YouTube.Repository.SubscriptionRepository;

namespace YouTube
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Unity
            var container = new UnityContainer();
            container.RegisterType<IVideoRepositoryStore, VideoRepositoryStore>(new HierarchicalLifetimeManager());
            container.RegisterType<ISubscriptionRepository, SubscriptionRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserRepositoryStore, UserRepositoryStore>(new HierarchicalLifetimeManager());
            container.RegisterType<ILikeRepositoryInterface, LikeRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ICommentRepository, CommentRepository>(new HierarchicalLifetimeManager());


            config.DependencyResolver = new UnityResolver(container);
            
          
        }
    }
}
