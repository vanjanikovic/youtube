﻿var token = null;
var headers = {};
var host = window.location.host;
var role = null;
var otherUserId;
var myId;
var videoId;
var info;

$(document).ready(function () {

    prikazNavigacije();
    loadVideos();

    //navigacija dugmad...
    $("body").on("click", "#btnLogin", prikazLogovanja);
    $("body").on("click", "#btnRegister", prikazRegistracije);
    $("body").on("click", "#btnHome", loadVideos);
    $("body").on("click", "#btnMe", getUser);
    $("body").on("click", "#btnLogout", logout);
    $("body").on("click", "#btnUsers", getAllUsers);
    $("body").on("click", "#btnAddVideo", addVideoPage);

    $("body").on("click", "#subscribe", subscribe);
    $("body").on("click", "#unsubscribe", unsubscribe);

    //ostalo..
    $("body").on("click", "#videoLink", getVideo);
    $("body").on("click", "#userLink", getUser);

    //logovanje i registracija
    $("body").on("submit", "#log", submitLog);
    $("body").on("submit", "#reg", submitReg);
    $("body").on("submit", "#comment", submitComment);

   

});

function submitLog(e) {
    e.preventDefault();
    var username = $("#logUsername").val();
    var password = $("#logPassword").val();

    var sendData = {
        "grant_type": "password",
        "username": username,
        "password": password
    };
    $.ajax({
        "type": "POST",
        "url": "https://" + host + "/Token",
        "data": sendData
    }).done(function (data) {
        token = data.access_token;
        loadVideos();
        prikazNavigacije(); 
    }).fail(function (data) {
        console.log(data);

    });
}
function submitReg(e) {
    e.preventDefault();
    console.log("submitovano");
    var email = $("#regEmail").val();
    var username = $("#regUsername").val();
    var password = $("#regPasword").val();
    var confirm = password;
    var sendData = {
        "Email": email,
        "Password": password,
        "ConfirmPassword": confirm,
        "UserName":username
    };
    $.ajax({
        "type": "POST",
        "url": "https://" + host + "/api/Account/Register",
        "data": sendData
    }).done(function (data) {
        console.log(data);    })
        .fail(function (data) {
        alert(data);
        });
    
}
function submitComment(e) {
    e.preventDefault();
    console.log("komentar...", videoId);
    var comm = $("#commText").val();
    var sendData = {
        "Text": comm,
        "VideoId": videoId
    };
    $.ajax({
        "type": "POST",
        "url": "https://" + host + "/api/comm",
        "data": sendData,
        "headers": { "Authorization": "bearer " + token }
    }).done(function (data) {
        reloadVideo();
    })
        .fail(function (data) {
            alert(data);
        });

}
function logout(e) {
    e.preventDefault();

}
function getVideo(e) {
    e.preventDefault();
    var id = this.name;
    $("#content").empty();
    var url = "https://" + host + "/api/video/" + id;
    if (token == null) {
        $.getJSON(url, setVideoInfo);
    }
    else {
        $.ajax({
            "type": "GET",
            "url": url,
            "headers": { "Authorization": "bearer " + token }
        }).done(function (data) {
            otherUserId = data.UserId;
            videoId = data.Id;
            setVideoInfo(data);
            info = "videoPage";
        }).fail(function (data) {
            console, log(data);
        });
    }
}
function reloadVideo() {
    
    $("#content").empty();
    var url = "https://" + host + "/api/video/" + videoId;
    if (token == null) {
        $.getJSON(url, setVideoInfo);
    }
    else {
        $.ajax({
            "type": "GET",
            "url": url,
            "headers": { "Authorization": "bearer " + token }
        }).done(function (data) {
            otherUserId = data.UserId;
            videoId = data.Id;
            setVideoInfo(data);
        }).fail(function (data) {
            console, log(data);
        });
    }
}
function getUser(e) {
    e.preventDefault();
    var id = this.name;
    $("#content").empty();

    var url = "https://" + host + "/api/userProfile/" + id;
    $.ajax({
        "type": "GET",
        "url": url,
        "headers": { "Authorization": "bearer " + token }
    }).done(function (data) {
        otherUserId = data.Id;
        setUserInfo(data);
        info = "userPage";
    }).fail(function (data) {
        console.log(data);
    });
   
}
function reloadUser(){
    $("#content").empty();

    var url = "https://" + host + "/api/userProfile/" + otherUserId;
    $.ajax({
        "type": "GET",
        "url": url,
        "headers": { "Authorization": "bearer " + token }
    }).done(function (data) {
        otherUserId = data.Id;
        setUserInfo(data);
    }).fail(function (data) {
        console.log(data);
    });
}
function reloadPage() {
    if (info === "userPage") {
        reloadUser();
    }
    else if (info === "videoPage") {
        reloadVideo();
    }
}
function getAllUsers(e) {
    e.preventDefault();
    console.log("deo adminovog navbara");
}
function prikazNavigacije() {
    if (token === null) {
        var nav = "<button class='btn btn-outline-info nav1' id='btnLogin'>login</button>" +
            "<button class='btn btn-info nav1' id='btnRegister'>register</button>";
        $("#nav").empty().append(nav);

    }
    else {
        if (role === "admin") {
            var nav = "<button class='btn btn-outline-danger nav1' id='btnMe'>My profile</button>" +
                "<button class='btn btn-outline-danger nav1' id='btnHome'>Home</button>" +
                "<button class='btn btn-outline-danger nav1' id='btnUsers'>Users</button>" +
                "<button class='btn btn-outline-danger nav1' id='btnAddVideo'>Add Video</button>" +
                "<button class='btn btn-danger nav1' id='btnLogout'>Logout</button>";
            $("#nav").empty().append(nav);
        }
        else {
            var nav = "<button class='btn btn-outline-danger nav1' id='btnMe'>My profile</button>" +
                "<button class='btn btn-outline-danger nav1' id='btnHome'>Home</button>" +
                "<button class='btn btn-outline-danger nav1' id='btnAddVideo'>Add Video</button>" +
                "<button class='btn btn-danger nav1' id='btnLogout'>Logout</button>";
            $("#nav").empty().append(nav);
        }

    }

}

function prikazLogovanja() {
    var login = "<div class='col-lg-4 col-sm-6  forma'><form id='log'><h2>Login</h2><div class='form-group'><label>Username</label>" +
        "<input type='text' id='logUsername' class='form-control'/></div>" +
        "<div class='form-group'><label>Password</label>" +
        "<input type='password' id='logPassword' class='form-control'/></div>" +
        "<button type='submit' class='btn btn-outline-danger'>login</button>"+ "</form></div>";
    $("#content").empty().append(login);
}
function prikazRegistracije() {
    var register = "<div class='col-lg-4 col-sm-6 forma'><form id='reg'><h2>Register</h2><div class='form-group'><label>Email</label>" +
        "<input type='text' id='regEmail' class='form-control'/></div>" +
        "<div class='form-group'><label>Username</label><input type='text' class='form-control' id='regUsername'></div>"+
        "<div class='form-group'><label>Password</label>" +
        "<input type='password' id='regPassword' class='form-control'/></div>" +
        "<button type='submit' class='btn btn-outline-danger'>register</button>"+"</form></div>";
    $("#content").empty().append(register);
}

function loadVideos() {
    var url = "https://" + host + "/api/Videos";
    if (token === null) {
        $.getJSON(url, setVideos);
    }
    else {
        $.ajax({
            "type": "GET",
            "url": url,
            "headers": { "Authorization": "bearer " + token }
        }).done(function (data) {
            setVideos(data);
        }).fail(function (data) {
            console, log(data);
        });
    }
}
function setVideos(data) {
    $("#content").empty();
    //proveravamo odmah da li je admin zbog role
    if (data[0].Info === "admin") {
        role = "admin";
    }
    prikazNavigacije();
    //postavljamo videe
    for (let i = 0; i < data.length; i++) {
        var div = $("<div class='col-lg-10'></div>");
        var card = $("<div class='card border-info' style='max-width:80rem;'></div>");
        var informacije = "<div class='card-body text-info'><h3 class='card-title'><a id='videoLink' href='' name="+data[i].Id+">" + data[i].Name + "</a></h3>" +
            "<h5 class='card-title'><a href='' id='userLink' name="+data[i].UserId+" >"+data[i].UserName+"</a></h5>"+
            "<p class='card-text small'>" + data[i].Rating + " ~ " + data[i].DateCreated + "</p>" + "</div>";
        card.append(informacije);
        div.append(card);
        $("#content").append(div);
    }
}
function setVideoInfo(data) {
    console.log(data);
    if (data.Info === "anonymous") {
        var informacije = "<div class='jumbotron m-3 forma'><h2>" + data.Name + "</h2>" +
            "<h4><a href='' id='userLink' name=" + data.UserId + ">" + data.Username + "</a><h4>" +
            "<p>" + (data.Description ? data.Description : " ") + "</p>" +
            "<p class='small'>views " + data.Rating+" ~ "+data.DateCreated + "</p>" + "</div>";
        $("#content").append(informacije);
        var video = "<div class='videoSrc'><iframe width='560' height='315' src = " +data.VideoSrc + " frameborder='0'  allow ='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen ></iframe ></div>";
        $("#content").append(video);

        //ovde ide slanje za komentare i lajkove
        if (data.AllowRating) {
            $.ajax({
                "type": "GET",
                "url": "https://" + host + "/api/videoRating/" + data.Id
            }).done(function (d) {
                setRating(d);
            }).fail(function (d) {
                console.log(d);
            });
        }
        if (data.AllowComments) {
            $.ajax({
                "type": "GET",
                "url": "https://" + host + "/api/comment/" + data.Id
            }).done(function (d) {
                setComments(d);
            }).fail(function (d) {
                console.log(d);
            });
        }
    }
    else {
        if (data.Info === "admin") {
            var informacije = "<div class='jumbotron m-3'><h4>" + data.Name + "</h4>" +
                "<h6><a href='' id='userLink' name=" + data.UserId + ">" + data.Username + "</a><h6>" +
                "<p>" + (data.Description ? data.Description : " ") + "</p>" +
                "<p class='small'>views " + data.Rating + " ~ " + data.DateCreated + "</p>" + "</div>";
            $("#content").append(informacije);
            var video = "<iframe width='560' height='315' src = " + data.VideoSrc + " frameborder='0'  allow ='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen ></iframe >";
            $("#content").append(video);
        }
        else {
            if (data.Info === "owner") {
                var informacije = "<div class='jumbotron m-3 forma'><h2>" + data.Name + "</h2>" +
                    "<h4><a href='' id='userLink' name=" + data.UserId + ">" + data.Username + "</a><h4>" +
                    "<p>" + (data.Description ? data.Description : " ") + "</p>" +
                    "<p class='small'>views " + data.Rating + " ~ " + data.DateCreated + "</p>" + "</div>";
                $("#content").append(informacije);
                var video = "<div class='videoSrc'><iframe width='560' height='315' src = " + data.VideoSrc + " frameborder='0'  allow ='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen ></iframe ></div>";
                $("#content").append(video);

                
                //poziv da vidimo rejting
                    $.ajax({
                        "type": "GET",
                        "url": "https://" + host + "/api/videoRating/" + data.Id
                    }).done(function (d) {
                        setRating(d);
                    }).fail(function (d) {
                        console.log(d);
                    });

                
                    //poziv da vidimo sve komentare
                    $.ajax({
                        "type": "GET",
                        "url": "https://" + host + "/api/comment/" + data.UserId
                    }).done(function (d) {
                        setComments(d);
                    }).fail(function (d) {
                        console.log(d);
                    });
                

               
            }
            else if (data.Info === "user") {
                var informacije = "<div class='jumbotron m-3 forma'><h2>" + data.Name + "</h2>" +
                    "<h4><a href='' id='userLink' name=" + data.UserId + ">" + data.Username + "</a><h4>" +
                    "<p>" + (data.Description ? data.Description : " ") + "</p>" +
                    "<p class='small'>views " + data.Rating + " ~ " + data.DateCreated + "</p>" + "</div>";
                $("#content").append(informacije);
                var video = "<div class='videoSrc'><iframe width='560' height='315' src = " + data.VideoSrc + " frameborder='0'  allow ='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen ></iframe ></div>";
                $("#content").append(video);

                //brij lajkova i da li smo lajkovali
                if (data.AllowRating) {
                    $.ajax({
                        "type": "GET",
                        "url": "https://" + host + "/api/videoRating/" + data.Id
                    }).done(function (d) {
                        setRating(d);
                    }).fail(function (d) {
                        console.log(d);
                    });
                  
                }
                if (data.AllowComments) {
                    $.ajax({
                        "type": "GET",
                        "url": "https://" + host + "/api/comment/" + data.Id
                    }).done(function (d) {
                        setComments(d);
                    }).fail(function (d) {
                        console.log(d);
                    });

                    //posto su dozvoljeni komentari mozemo da komentarisemo :)
                    //ovo je forma
                    var forma = "<div class='row justufy-content-center m-4 text-white'><dic class='col-sm-12'><form id='comment' action='' method='POST' class='form-inline'><div class='form-group col-8'>" +
                        "<label class='col-3'>Add comment</label><input " +
                        "type='text' class='form-control col-9' id='commText'/></div>" +
                        "<button type='submit' class='btn btn-outline-info m-2'>Submit</button></form></div></div > ";
                    $("#content").append(forma);

                }

                //poziv da vidimo da li smo subscribovani
                $.ajax({
                    "type": "GET",
                    "url": "https://" + host + "/api/isSubscribed/" + data.UserId,
                    "headers": { "Authorization": "bearer " + token }
                }).done(function (d) {
                    setSubBotton(d);
                }).fail(function (d) {
                    console.log(d);
                });
            }
            else {

            }
               
            
        }
    }
}
function setRating(data) {
    if (!token) {
        var info = "<div class='container'><p class='text-white text-center' style='letter-spacing:10px'><i id='like' class='far fa-thumbs-up'></i> " + data.Likes + " ~ <i class='far fa-thumbs-down' id='dislike'></i> " + data.Dislikes + "</p></div>";
        $("#content").append(info);
    }
    else {
        var info = "<div class='container'><p class='text-white text-center' style='letter-spacing:10px'><i id='like' class='far fa-thumbs-up'></i> " + data.Likes + " ~ <i class='far fa-thumbs-down' id='dislike'></i> " + data.Dislikes + "</p></div>";
        $("#content").append(info);
    }
}
function setComments(data) {
    for (let i = 0; i < data.length; i++) {
        var div = $("<div class='container text-white comm'></div>");
        var informacije = "<h6><a href=''  id='userLink' name=" + data[i].UserId + ">" + data[i].Username + "</a></h6>" +
            "<p class='text'>" + data[i].Text + "</p><p class='text-info'>" + data[i].DateCreated + " ~ Likes: " + data[i].Likes +
            " ~ Dislikes: " + data[i].Dislikes + "</p>";
        div.append(informacije);
        $("#content").append(div);
    }
}
function setUserInfo(data) {
    if (data.Info === "anonymous") {
        var informacije = "<div class='jumbotron m-3 forma'><h2>" + data.Username + "</h2>" +
            "<p>" + (data.Description ? data.Description : " ") + "</p>" +
            "<p class='small'>Subscribed: " + data.NumberOfSubs + " ~ " + data.CreatedDate + "</p>" + "</div>";
        $("#content").append(informacije);

        //poziv za listu videa
        $.ajax({
            "type": "GET",
            "url": "https://" + host + "/api/userVideos/" + data.Id
        }).done(function (d) {
            setUserVideos(d);
        }).fail(function (d) {
            console.log(d);
        });
        //poziv za sve subs
        $.ajax({
            "type": "GET",
            "url": "https://" + host + "/api/subs/" + data.Id
        }).done(function (d) {
            setUserSubs(d);
        }).fail(function (d) {
            console.log(d);
        });
        
    }
    else {
        if (data.Info === "admin") {
            var informacije = "<div class='jumbotron m-3'><h4>" + data.Username + "</h4>" +
                "<p>" + (data.Description ? data.Description : " ") + "</p>" +
                "<p class='small'>Subscribed: " + data.NumberOfSubs + " ~ " + data.CreatedDate + "</p>" + "</div>";
            $("#content").append(informacije);
        }
        else {
            if (data.Info === "owner") {
                var informacije = "<div class='jumbotron m-3'><h4>" + data.Username + "</h4>" +
                    "<p>" + (data.Description ? data.Description : " ") + "</p>" +
                    "<p class='small'>Subscribed: " + data.NumberOfSubs + " ~ " + data.CreatedDate + "</p>" + "</div>";
                $("#content").append(informacije);
            }
            else {
                if (data.Info === "user") {
                    var informacije = "<div class='jumbotron m-3 forma'><h2>" + data.Username + "</h2>" +
                        "<p>" + (data.Description ? data.Description : " ") + "</p>" +
                        "<p class='small'>Subscribed: " + data.NumberOfSubs + " ~ " + data.CreatedDate + "</p>" + "</div>";
                    $("#content").append(informacije);

                    //poziv za listu videa
                    $.ajax({
                        "type": "GET",
                        "url": "https://" + host + "/api/userVideos/" + data.Id
                    }).done(function (d) {
                        setUserVideos(d);
                    }).fail(function (d) {
                        console.log(d);
                    });
                    //poziv za sve subs
                    $.ajax({
                        "type": "GET",
                        "url": "https://" + host + "/api/subs/" + data.Id
                    }).done(function (d) {
                        setUserSubs(d);
                    }).fail(function (d) {
                        console.log(d);
                    });

                    //poziv da vidimo da li smo subscribovani
                    $.ajax({
                        "type": "GET",
                        "url": "https://" + host + "/api/isSubscribed/" + data.Id,
                        "headers": { "Authorization": "bearer " + token }
                    }).done(function (d) {
                        setSubBotton(d);
                    }).fail(function (d) {
                        console.log(d);
                    });
                }
                else {
                    //ovde ide deo kad je blokiran...
                }
            }
        }
    }
}
function setUserVideos(data) {
    for (let i = 0; i < data.length; i++) {
        var div = $("<div class='container'></div>");
        var card = $("<div class='card border-info m-3' style='max-width:30rem;'></div>");
        var informacije = "<div class='card-body text-info'><h5 class='card-title'><a id='videoLink' href='' name=" + data[i].Id + ">" + data[i].Name + "</a></h5>" +
            "<h6class='card-title'><a href='' id='userLink' name=" + data[i].UserId + " >" + data[i].UserName + "</a></h6>" +
            "<p class='card-text small'>" + data[i].Rating + " ~ " + data[i].DateCreated + "</p>" + "</div>";
        card.append(informacije);
        div.append(card);
        $("#content").append(div);
    }
}
function setUserSubs(data) {
    for (let i = 0; i < data.length; i++) {
        var div = $("<div class='container m-2 text-danger text-center'></div>");
        var informacije = "<h6><a href='' id=userLink name=" + data[i].UserId + ">" + data[i].UserName + "</a> " +
            "~ " + data[i].NumberOfSubscriptions + "</h=6>";
        div.append(informacije);
        $("#content").append(div);
    }
}
function setSubBotton(data) {
    if (data === true) {
        var button = "<button class='btn btn-outline-info' id='unsubscribe'>Unsubscribe</button>";
        $(".forma").append(button);

    }
    if (data === false) {
        var button = "<button class='btn btn-danger' id='subscribe'>Subsrcibe</button>";
        $(".forma").append(button);
    }
}

function subscribe(e) {
    e.preventDefault();
    var url = "https://" + host + "/api/Subscribe/" + otherUserId;
    $.ajax({
        "type": "POST",
        "url": url,
        "headers": { "Authorization": "bearer " + token }
    }).done(function (data) {

        reloadPage();
    }).fail(function (data) {
        console.log(data);
    });
    
}

function unsubscribe(e) {
    e.preventDefault();
    var url = "https://" + host + "/api/unsubscribe/" + otherUserId;
    $.ajax({
        "type": "DELETE",
        "url": url,
        "headers": { "Authorization": "bearer " + token }
    }).done(function (data) {
        reloadPage();
    }).fail(function (data) {
        console.log(data);
    });
}

function addVideoPage() {
    $("#content").empty();
    var forma = "<div class='row  text-white forma add'><div class='col-lg-10'><form>" +
        "<h2>add new video</h2><div class='form-group'><label>Name</label><input " +
        "type='text' class='form-control col-12' /></div>" +
        "<div class='form-group'><label>Description</label>" +
        "<input type='text' class='form-control col-12'/></div>" +
        "<div class='form-group'><label>Video...</label>" +
        "<input type='text' class='form-control col-12'/></div>" +
        "<button class='btn btn-outline-danger'>submit</button>" +
        "</form></div></div>";
    $("#content").append(forma);
}
