namespace YouTube.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using YouTube.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<YouTube.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(YouTube.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            Visibility visibility1 = new Visibility() { Id = 1, Name = "public" };
            Visibility visibility2 = new Visibility() { Id = 2, Name = "private" };
            Visibility visibility3 = new Visibility() { Id = 3, Name = "unlisted" };
            context.Visibilities.AddOrUpdate(visibility1, visibility2, visibility3);
            context.SaveChanges();


            var menadzer = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleMenadzer = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            var user1 = new ApplicationUser()
            {
                Email = "pera@mail.com",
                EmailConfirmed = true,
                UserName = "Pera",
                FirstName = "Petar",
                LastName = "Petrovic",
                Description = " Information about Petar..",
                JoinDate = DateTime.Now,
                IsBlocked = false,
                IsDeleted=false
            };
            menadzer.Create(user1, "Sifra.123");
           
            var user2 = new ApplicationUser()
            {
                Email = "mika@mail.com",
                EmailConfirmed = true,
                UserName = "Mika",
                FirstName = "Mika",
                LastName = "Mikic",
                Description = " Information about Mika..",
                JoinDate = DateTime.Now,
                IsBlocked = false,
                IsDeleted = false
            };
            menadzer.Create(user2, "Sifra.123");
           
            var user3 = new ApplicationUser()
            {
                Email = "zika@mail.com",
                EmailConfirmed = true,
                UserName = "Zika",
                FirstName = "Zika",
                LastName = "Zikic",
                Description = " Information about Zika..",
                JoinDate = DateTime.Now,
                IsBlocked = false,
                IsDeleted = false
            };
            menadzer.Create(user3, "Sifra.123");
            var userAdmin1 = new ApplicationUser()
            {
                Email = "nikola@mail.com",
                EmailConfirmed = true,
                UserName = "nikola",
                FirstName = "Nikola",
                LastName = "Nikolic",
                Description = " Information about Nikola..",
                JoinDate = DateTime.Now,
                IsBlocked = false,
                IsDeleted = false
            };
            menadzer.Create(userAdmin1, "Sifra.123");
           
            var userAdmin2 = new ApplicationUser()
            {
                Email = "vuk@mail.com",
                EmailConfirmed = true,
                UserName = "Vuk",
                FirstName = "Vuk",
                LastName = "Vukic",
                Description = " Information about Vuk..",
                JoinDate = DateTime.Now,
                IsBlocked = false,
                IsDeleted = false
            };
            menadzer.Create(userAdmin2, "Sifra.123");

            if (roleMenadzer.Roles.Count() == 0)
            {
                roleMenadzer.Create(new IdentityRole() { Name = "User" });
                roleMenadzer.Create(new IdentityRole() { Name = "Admin" });

            }


            var admin1 = menadzer.FindByName("nikola");
            menadzer.AddToRoles(admin1.Id, new string[] { "User", "Admin" });

            var admin2 = menadzer.FindByName("vuk");
            menadzer.AddToRoles(admin2.Id, new string[] { "User", "Admin" });
            
            var justAUser1 = menadzer.FindByName("Pera");
            menadzer.AddToRoles(justAUser1.Id, new string[] { "User"}); 
            
            var justAUser2 = menadzer.FindByName("Mika");
            menadzer.AddToRoles(justAUser2.Id, new string[] { "User"});

            var justAUser3 = menadzer.FindByName("Zika");
            menadzer.AddToRoles(justAUser3.Id, new string[] { "User" });


            Video video1 = new Video() 
            {
                Id=1,
                Name= "Muse - Uprising [Official Video]",
                VideoSrc= "https://www.youtube.com/embed/w8KQmps-Sog",
                VisibilityId=1,
                AllowComments=true,
                AllowRating=true,
                IsBlocked=false,
                IsDeleted=false,
                DateCreated=DateTime.Now,
                UserId=admin1.Id
            };

            Video video2 = new Video()
            {
                Id = 2,
                Name = "Gorillaz - Feel Good Inc. (Official Video)",
                VideoSrc = "https://www.youtube.com/embed/HyHNuVaZJ-k",
                VisibilityId = 2,
                AllowComments = true,
                AllowRating = true,
                IsBlocked = false,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                UserId = admin1.Id

            };

            Video video3 = new Video()
            {
                Id = 3,
                Name = "Gorillaz - DARE (Official Video)",
                VideoSrc = "https://www.youtube.com/embed/uAOR6ib95kQ",
                VisibilityId = 1,
                AllowComments = true,
                AllowRating = true,
                IsBlocked = false,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                UserId=justAUser1.Id
            };

            Video video4 = new Video()
            {
                Id = 4,
                Name = "Imagine Dragons - Believer",
                VideoSrc = "https://www.youtube.com/embed/7wtfhZwyrcc",
                VisibilityId = 2,
                AllowComments = true,
                AllowRating = true,
                IsBlocked = false,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                UserId = justAUser1.Id

            };

            Video video5 = new Video()
            {
                Id = 5,
                Name = "2Pac - Changes (Official Music Video) ft. Talent",
                VideoSrc = "https://www.youtube.com/embed/eXvBjCO19QY",
                VisibilityId = 1,
                AllowComments = true,
                AllowRating = true,
                IsBlocked = false,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                UserId=justAUser2.Id
            };

            Video video6 = new Video()
            {
                Id = 6,
                Name = "Muse - Resistance",
                VideoSrc = "https://www.youtube.com/embed/TPE9uSFFxrI",
                VisibilityId = 1,
                AllowComments = true,
                AllowRating = true,
                IsBlocked = false,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                UserId = justAUser2.Id

            };

            Video video7 = new Video()
            {
                Id = 7,
                Name = "Depeche Mode - Enjoy The Silence (Official Video)",
                VideoSrc = "https://www.youtube.com/embed/aGSKrC7dGcY",
                VisibilityId = 2,
                AllowComments = true,
                AllowRating = true,
                IsBlocked = false,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                UserId = justAUser2.Id

            };

            Video video8 = new Video()
            {
                Id = 8,
                Name = "Sting - Desert Rose (Official Video)",
                VideoSrc = "https://www.youtube.com/embed/C3lWwBslWqg",
                VisibilityId = 1,
                AllowComments = true,
                AllowRating = true,
                IsBlocked = false,
                IsDeleted = false,
                DateCreated = DateTime.Now,
                UserId = justAUser2.Id

            };
            context.Videos.AddOrUpdate(video1,video2,video3,video4,video5,video6,video7,video8);
            context.SaveChanges();

            Subscription subscription1 = new Subscription()
            {
                Id=1,
                UserId=admin1.Id,
                SubscribedUserId=justAUser3.Id

            };
            Subscription subscription2 = new Subscription()
            {
                Id = 2,
                UserId = admin2.Id,
                SubscribedUserId = justAUser3.Id

            };
            Subscription subscription3 = new Subscription()
            {
                Id = 3,
                UserId = justAUser1.Id,
                SubscribedUserId = justAUser3.Id

            };
            Subscription subscription4 = new Subscription()
            {
                Id = 4,
                UserId = justAUser2.Id,
                SubscribedUserId = justAUser3.Id

            };

            Subscription subscription5 = new Subscription()
            {
                Id = 5,
                UserId = admin1.Id,
                SubscribedUserId = justAUser2.Id

            };
            Subscription subscription6 = new Subscription()
            {
                Id = 6,
                UserId = admin2.Id,
                SubscribedUserId = justAUser2.Id

            };
            Subscription subscription7 = new Subscription()
            {
                Id = 7,
                UserId = justAUser1.Id,
                SubscribedUserId = justAUser2.Id

            };
            Subscription subscription8 = new Subscription()
            {
                Id = 8,
                UserId = justAUser3.Id,
                SubscribedUserId = justAUser2.Id

            };

            Subscription subscription9 = new Subscription()
            {
                Id = 9,
                UserId = admin1.Id,
                SubscribedUserId = justAUser1.Id

            };
            Subscription subscription10 = new Subscription()
            {
                Id = 10,
                UserId = justAUser2.Id,
                SubscribedUserId = justAUser1.Id

            };
            context.Subscriptions.AddOrUpdate(subscription1, subscription2, subscription3, subscription4, subscription5, subscription6, subscription7, subscription8, subscription9, subscription10);
            context.SaveChanges();

            Comment comment1 = new Comment() 
            { 
                Id=1,
                Text= "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo",
                UserId=justAUser1.Id,
                VideoId=1,
                DateCreated=DateTime.Now,
                IsDeleted=false
            };
            Comment comment2 = new Comment()
            {
                Id = 2,
                Text = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                UserId = justAUser2.Id,
                VideoId = 1,
                DateCreated = DateTime.Now,
                IsDeleted = false
            };
            Comment comment3 = new Comment()
            {
                Id = 3,
                Text = "Aliquam tincidunt mauris eu risus.",
                UserId = admin1.Id,
                VideoId = 1,
                DateCreated = DateTime.Now,
                IsDeleted = false
            };
            context.Comments.AddOrUpdate(comment1, comment2, comment3);
            context.SaveChanges();

            Like likeVideo1 = new Like() { 
                Id=1,
                IsLike=true,
                DateCreated=DateTime.Now,
                VideoId=1,
                UserId=admin1.Id,
                
                
            };
            Like likeVideo2 = new Like()
            {
                Id = 2,
                IsLike = true,
                DateCreated = DateTime.Now,
                VideoId = 1,
                UserId = admin2.Id
            };
            Like likeVideo3 = new Like()
            {
                Id = 1,
                IsLike = true,
                DateCreated = DateTime.Now,
                VideoId = 1,
                UserId = justAUser1.Id
            };
            Like likeVideo4 = new Like()
            {
                Id = 1,
                IsLike = false,
                DateCreated = DateTime.Now,
                VideoId = 1,
                UserId = justAUser2.Id
            };
            Like likeVideo5 = new Like()
            {
                Id = 1,
                IsLike = false,
                DateCreated = DateTime.Now,
                VideoId = 1,
                UserId = justAUser3.Id
            };
            Like likeComm1 = new Like() {
                IsLike=true,
                DateCreated=DateTime.Now,
                CommentId=1,
                UserId=admin1.Id
            };
            Like likeComm2 = new Like()
            {
                IsLike = true,
                DateCreated = DateTime.Now,
                CommentId = 1,
                UserId = admin2.Id
            };
            Like likeComm3 = new Like()
            {
                IsLike = false,
                DateCreated = DateTime.Now,
                CommentId = 1,
                UserId = justAUser1.Id
            };
            context.Likes.AddOrUpdate(likeVideo1, likeVideo2, likeVideo3, likeVideo4, likeVideo5, likeComm1, likeComm2, likeComm3);
            context.SaveChanges();

        }
    }
}
