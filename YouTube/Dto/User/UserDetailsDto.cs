﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouTube.Dto.User
{
    public class UserDetailsDto:UserDto
    {
        public string Username { get; set; }
        public int NumberOfSubs { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Description { get; set; }
    }
}