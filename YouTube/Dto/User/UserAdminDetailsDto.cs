﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouTube.Dto.User
{
    public class UserAdminDetailsDto:UserDetailsDto
    {
       
        public string Role { get; set; }

    }
}