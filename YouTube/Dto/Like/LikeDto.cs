﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouTube.Dto.Like
{
    public class LikeDto
    {
        public int VideoOrCommentId { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }

    }
}