﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouTube.Dto.Comment
{
    public class CommentDto
    {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Text { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }


    }
}