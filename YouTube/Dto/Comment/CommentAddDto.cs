﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouTube.Dto.Comment
{
    public class CommentAddDto
    {
        public string Text { get; set; }
        public int VideoId { get; set; }

    }
}