﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YouTube.Dto.Video
{
    public class VideoUpdateDto
    {
        [Required]
        public int Id { get; set; }
        public string Description { get; set; }
        public int VisibilityId { get; set; }
        public bool AllowComments { get; set; }
        public bool AllowRating { get; set; }

    }
}