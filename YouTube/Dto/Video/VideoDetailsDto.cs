﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouTube.Dto.Video
{
    public class VideoDetailsDto
    {
        public string Info { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string VideoSrc { get; set; }
        public string ThumbnailSrc { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsBlocked { get; set; }
        public bool AllowComments { get; set; }
        public bool AllowRating { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public int Rating { get; set; }
        public DateTime DateCreated { get; set; }
        public string Description { get; set; }
    }
}