﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YouTube.Dto.Video
{
    public class VideoAddDto
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string VideoSrc { get; set; }
        public string ThumbnailSrc { get; set; }
        public string Description { get; set; }
        public int VisibilityId { get; set; }

    }
}