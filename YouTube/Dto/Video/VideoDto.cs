﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouTube.Dto.Video
{
    public class VideoDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int Rating { get; set; }
        public DateTime DateCreated { get; set; }
        public string ThumbnailSrc { get; set; }
        public string Info { get; set; }

    }
}