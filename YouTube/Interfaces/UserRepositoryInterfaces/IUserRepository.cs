﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.Dto.User;

namespace YouTube.Interfaces.UserRepositoryInterfaces
{
    public interface IUserRepository
    {
        UserDto GetUserById(string userId, string thisUserId);
    }
}
