﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.Dto.Like;

namespace YouTube.Interfaces.LikeRepositoryInterfaces
{
    public interface ILikeRepositoryInterface
    {
        LikeDto GetVideoRating(int videoId);
    }
}
