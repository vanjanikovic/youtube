﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.Dto.Comment;

namespace YouTube.Interfaces.CommentRepositoryInterfaces
{
   public interface ICommentRepository
    {
        IEnumerable<CommentDto> GetCommentsForVideo(string userIdentity,string thisUserId,int videoId);
        bool AddComment(string thisUserId, CommentAddDto comment);
    }
}
