﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.Dto.Video;

namespace YouTube.Interfaces.VideoRepositoryInterfaces
{
    public interface IVideoRepository
    {
        IEnumerable<VideoDto> GetAll(string userId);
        IEnumerable<VideoDto> GetAllUserVideos(string userId,string thisUserId);
        VideoDetailsDto GetVideo(string userId, int videoId);
        


    }
}
