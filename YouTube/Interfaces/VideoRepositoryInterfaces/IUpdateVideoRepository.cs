﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.Dto.Video;

namespace YouTube.Interfaces.VideoRepositoryInterfaces
{
    public interface IUpdateVideoRepository
    {
        void UpdateVideo(string userId, VideoUpdateDto video);
    }
}
