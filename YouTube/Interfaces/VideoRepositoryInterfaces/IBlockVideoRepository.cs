﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YouTube.Interfaces.VideoRepositoryInterfaces
{
    public interface IBlockVideoRepository
    {
        void BlockOrUnblock(int videoId);
    }
}
