﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.Dto.Subscription;

namespace YouTube.Interfaces.SubscriptionRepositoryInterfaces
{
    public interface ISubscriptionRepository
    {
        IEnumerable<SubscriptionDto> GetAllSubs(string userId);
        bool IsSubscribed(string userId, string thisUserId);
        bool Subscribe(string userId, string thisUserId);
        void Unsubscribe(string userId, string thisUserId);
    }
}
