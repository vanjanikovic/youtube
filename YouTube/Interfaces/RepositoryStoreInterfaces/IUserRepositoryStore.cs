﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.Dto.User;

namespace YouTube.Interfaces.RepositoryStoreInterfaces
{
    public interface IUserRepositoryStore
    {
        UserDto GetUserById(string userIdentity, string userId, string thisUserId);
        bool UpdateUser(string userIdentity, string thisUserId, string userId, UserUpdateDto user);
        bool BlockOrUnblock(string userIdentity, string userId);
        IEnumerable<UsersDto> GetallUsers(string userIdentity);
    }
}
