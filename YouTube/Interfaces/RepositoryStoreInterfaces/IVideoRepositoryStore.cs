﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YouTube.Dto.Video;
using YouTube.Interfaces.VideoRepositoryInterfaces;

namespace YouTube.Interfaces.RepositoryStoreInterfaces
{
    public interface IVideoRepositoryStore
    {
        IEnumerable<VideoDto> GetAll(string userIdentity, string userId);
        IEnumerable<VideoDto> GetAllUserVideos(string userIdentity, string userId,string thisUserId);
        VideoDetailsDto GetVideo(string userIdentity, string userId, int videoId);
        void AddVideo( string userIdentity,string userId, VideoAddDto video);
        void UpdateVideo(string userIdentiry, string userId, VideoUpdateDto video);
        void DeleteVideo(string userIdentity, string userId, int videoId);
        void BlockVideo(string userIdentity, int videoId);

    }
}
