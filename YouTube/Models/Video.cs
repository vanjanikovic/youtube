﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YouTube.Models
{
    public class Video
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string VideoSrc { get; set; }
        public string ThumbnailSrc { get; set; }
        public string Description { get; set; }
        [ForeignKey("Visibility")]
        public int? VisibilityId { get; set; }
        public Visibility Visibility { get; set; }
        public bool AllowComments { get; set; }
        public bool AllowRating { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsDeleted { get; set; }
        public int Rating { get; set; }
        public DateTime DateCreated { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}