﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YouTube.Models
{
    public class Like
    {
        public int Id { get; set; }
        public bool IsLike { get; set; }
        public DateTime DateCreated { get; set; }
        [ForeignKey("Video")]
        public int? VideoId { get; set; }
        public Video Video { get; set; }
        [ForeignKey("Comment")]
        public int? CommentId { get; set; }
        public Comment Comment { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}