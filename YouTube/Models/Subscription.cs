﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace YouTube.Models
{
    public class Subscription
    {
        public int Id { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        [ForeignKey("SubscribedUser")]
        public string SubscribedUserId { get; set; }
        public ApplicationUser SubscribedUser { get; set; }
    }
}